export default (issue) => {

  let label = issue.labels.nodes.find(({ title }) => title.match(/^workflow::/))?.title ||
    "no workflow";
  label = label.replace(/workflow::/, "");
  label = label.replace(/development/, "dev");
  label = label.toLowerCase();
  return label;
}
