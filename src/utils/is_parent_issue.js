import issueHasLabel from "@/utils/issue_has_label";

import getWorkflowLabel from "@/utils/get_workflow_label";

export default (issue = {}) => {
  // Parent issue if not in Design and has the [parent issue] title or both frontend/backend labels
  const isDesignIssue = getWorkflowLabel(issue) == "design";
  const isParentIssue = !isDesignIssue &&
    (/\[parent issue\]/i.test(issue.title) ||
      (issueHasLabel(issue, 'frontend') && issueHasLabel(issue, 'backend')));
  return isParentIssue;
}