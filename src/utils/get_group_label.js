import issue_has_label from "./issue_has_label";

export default (issue) => {
  let label =
    issue.labels.nodes.find(({ title }) => title.match(/^group::/))?.title ||
    "";
  label = label.replace(/group::/, "");
  label = label.replace(/composition analysis/, "CA");
  label = label.replace(/static analysis/, "SAST");
  label = label.replace(/dynamic analysis/, "DAST");
  label = label.replace(/fuzz testing/, "DAST");
  label = label.replace(/threat insights/, "Threat Insights");

  // as of 13.12 we are grouping Fuzz Testing into DAST and using the category label
  if (issue_has_label(issue, "Category:Fuzz Testing")) {
    label = "Fuzz Testing";
  }

  return label;
};
